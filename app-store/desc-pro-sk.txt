----------
| Title: |
----------
GPX Viewer PRO - GPS trasy a významné body



---------------
| Promo text: |
---------------
Posuňte Váš výlet na ďalšiu úroveň! S GPX Viewer PRO môžete importovať Vaše obľúbené GPX trasy a cesty priamo z GPX a KML súborov. Pozývame Vás do sveta GPX Viewer PRO.



----------------
| Description: |
----------------
Máte svoje obľúbené GPX alebo KML trasy a cesty a chcete ich vidieť v detaile na Vašom iPhone a iPade, alebo iba chcete ísť do hôr a nahrať svoje dobrodružstvo? Nezáleží na tom aký typ cestovateľa ste, GPX Viewer PRO, ponúka toto všetko a ešte viac.

MOŽNOSŤ IMPORTOVU GPX A KML SÚBOROV
Importujte Vaše obĺúbené GPX a KML súbory priamo z Vášho telefónu a prehliadajte staršie trasy a cesty alebo objavujte nové horizonty Vašej krajiny a choďte sa pozrieť na neprebádané úseky. Zobrazte viacero trás a ciest na rôznych typoch máp. Preskúmajte a analyzujte štatistiky a grafy trás. Použite túto aplikáciu ako jednoduchý navigačný nástroj so sledovaním pozície, ktorý umožňujte nasledovanie GPS pozície a otáčanie mapy.

HLAVNÉ ČRTY
• Merajte trasy a cesty, rýchlosti, prevýšenia, dĺžky a rôzne iné parametre dôležité pre turistiku
• Môžete si zobraziť údajte v grafoch
• Importujte trasy, cesty a významné body z GPX alebo KML formátov a prehliadajte ich parametre
• Možnosť plánovať trasy pomocou našej bezplatnej služby Trackbook - https://trackbook.online
• Úložisko pre trasy, cesty a významné body priamo vo Vašom zariadení s možnosťou exportu do GPX alebo KML pre ďalšie použitie

POUŽÍVATEĽSKÉ ROZHRANIE
• Jednoduché a efektívne používateľské rozhranie
• Podpora pre tmavú a svetlú farebnú schému

ONLINE MAPY
• Široká škála online máp ako Apple Mapy, OpenStreetMap, OpenTopoMap, ÖPNVKarte, CyclOSM, Mapbox, HERE, Thunderforest, Maptiler

OFFLINE MAPY
• Detailné offline vektorové mapy pre celý svet založené na OpenStreetMap dátach
• Široká ponuka štýlov pre offline mapy od štýlov orientovaných na mesto až po outdoorové štýly, ukážky: https://go.vecturagames.com/offlineios
• Mesačné aktualizácie s vylepšenými dátami

NAHRÁVANIE TRASY
• Nahrávanie trás s rýchlosťou a nadmorskou výškou

JEDNODUCHÁ NAVIGÁCIA
• Sledovanie pozície pomocou nasledovania GPS a otáčania mapy
• Zobrazovanie rýchlosti, nadmorskej výšky a smeru

TRASY A CESTY
• Importuje trasy a cesty z gpx a kml súborov uložených na iCloud alebo v úložisku zariadenia
• Analyzujte informácie a štatistiky o trasách a cestách
• Prehliadajte grafy ako výškový profil a rýchlostný profil pre trasy a cesty
• Prehliadajte grafy aj pre ostatné dáta ako kadenciu, srdcový tep, výkon a teplotu vzduchu
• Nastavenie farby pre trasu a cestu

VÝZNAMNÉ BODY
• Importujte významné body z gpx a kml súborov uložených na iCloud alebo v úložisku zariadenia
• Detailné informácie
• Nastavenie farby a ikony významného bodu
• Pridávajte významné body pomocou gesta stlačenia a podržania

INTEGRÁCIA S TRACKBOOK
• Synchronizácia trás a významných bodov vytvorených na našej bezplatnej službe Trackbook - https://trackbook.online

GPX Viewer PRO je nástroj pre outdoorové aktivity, či už sa jedná o turistiku alebo cyklistiku, alebo pomocou našej bezplatnej služby Trackbook - https://trackbook.online

EULA: https://vecturagames.com/eula_gpxviewerproios



----------------------------
| Screenshot descriptions: |
----------------------------
1. Zobrazovanie trás a významných bodov z gpx a kml súborov
2. Offline vektorové mapy založené na OpenStreetMap
3. 3D mapy
4. Nahrávanie trasy
5. Nasledovanie trasy
6. Zobrazovanie detailov trasy
7. Pridávanie významných bodov
8. Informácie a štatistiky o trasách a významných bodoch
